extends KinematicBody2D

#Some of those might be useful I had to test a lot of things
export var default_max_speed := 500
onready var max_speed := default_max_speed
var acceleration = Vector2.ZERO
export var default_turn_speed := 0.01
onready var turn_speed := default_turn_speed
var speed := 500
var laser_scene : Object = preload("res://src/ACT/ACT_player_ship/laser.tscn")
var desired_direction := Vector2.ZERO

var velocity := Vector2.ZERO
func _physics_process(delta: float) -> void:
	var steer = Vector2.ZERO

	desired_direction = Input.get_vector("move_left","move_right","move_up","move_down") * speed
	if desired_direction == Vector2.ZERO:
		desired_direction = Vector2(cos(velocity.angle()), sin(velocity.angle())) * speed
		#This is so the ship still moves forward when I let go

	steer = (desired_direction - velocity).normalized() * turn_speed
	acceleration += steer
	velocity = acceleration * speed
	velocity = velocity.limit_length(100)
	velocity = move_and_slide(velocity)
	#The problem right now is that if you point in the opposite direction of velocity
	#You will have a length that gets shorter and not only is the ship turning
	#on itself, it starts having a way faster turn speed.
	#I was considering maybe lerp_angle() but this would add linear 
	#interpolation to the #turn speed I think?
	
	#Visuals, including ship rotation.
	$SpriteShip.rotation = velocity.angle()
	if sign(velocity.x) == 1:
		$SpriteShip.flip_v = false
	else:
		$SpriteShip.flip_v = true
	$SpriteShipGun2.look_at(get_global_mouse_position())
	update()

func _draw() ->void:
	draw_line(Vector2.ZERO, desired_direction, Color.aquamarine)
	draw_line(Vector2.ZERO, velocity,Color.red)
	draw_line(velocity, desired_direction,Color.orange)



#IGNORE ANYTHING PAST THIS




func _on_Timer_timeout() -> void:
	shoot()

func shoot():
	#add ship velocity to bullet so backing away isn't broken
	#shots should only collide at max speed
	#visual speed indicator shows when you're fast enough
	#but the speed has to be gun direction + ship direction velocity so if looking
	#back you shoot slower
	var laser_top = laser_scene.instance()
	laser_top.rotation = $SpriteShipGun2.rotation + 0.045
	laser_top.position = get_node("SpriteShipGun2/top_gun").global_position
	get_tree().get_root().add_child(laser_top)
	
	var laser_bot = laser_scene.instance()
	laser_bot.rotation = $SpriteShipGun2.rotation - 0.045
	laser_bot.position = get_node("SpriteShipGun2/bot_gun").global_position
	get_tree().get_root().add_child(laser_bot)

#func boost():
#	$Boost_Timer.start()
#	max_speed = default_max_speed * 1.5
#	speed = max_speed
#	if desired_direction == Vector2.ZERO:
#		desired_direction = Vector2(cos($SpriteShip.rotation), sin($SpriteShip.rotation))
#	velocity = max_speed * direction
#	turn_speed = 0.005
#	#Trying to turn lowers speed, how to deal with that?
#	#Prob needs to be its own code that moves regardless of direction
#	#impulse kind of thing



func _on_Boost_Timer_timeout() -> void:
	max_speed = default_max_speed
	turn_speed = default_turn_speed
