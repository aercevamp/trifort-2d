extends KinematicBody2D

#Some of those might be useful I had to test a lot of things
export var default_max_speed := 500
onready var max_speed := default_max_speed
var acceleration = Vector2.ZERO
export var default_turn_speed := 40.0
onready var turn_speed := default_turn_speed
var speed := 50
var laser_scene : Object = preload("res://src/ACT/ACT_player_ship/laser.tscn")
onready var boost_timer = get_node("Boost_Timer")
var current_direction := Vector2.RIGHT
var desired_direction := Vector2.ZERO
var desired_velocity := Vector2.ZERO
var stored_velocity := Vector2.RIGHT
var velocity := Vector2.RIGHT
var thrust := false


func _physics_process(delta: float) -> void:
	if  not Input.is_action_pressed("ui_down"):
		speed = lerp(speed, 10, 0.05)
		turn_speed = default_turn_speed * 1.5
		thrust = false
	else:
		speed = lerp(speed, default_max_speed, 0.10)
		turn_speed = default_turn_speed
		thrust = true
	desired_direction = Input.get_vector("move_left","move_right","move_up","move_down").normalized()

	if desired_direction != Vector2.ZERO:
		var angle = current_direction.angle_to(desired_direction)
		
		var angle_diff = clamp(angle, -0.1, 0.1) * turn_speed * delta
		#angle_diff = clamp(angle_diff, -abs(angle), abs(angle)) #what is this for?
		current_direction = current_direction.rotated(angle_diff)

	if thrust == true: desired_velocity =  current_direction * speed
	else: desired_velocity = velocity.normalized() * speed
	var steering_vector := desired_velocity - velocity
	velocity += steering_vector * 0.1
	velocity = move_and_slide(velocity)


	#Visuals, including ship rotation.
	$SpriteShip.rotation = current_direction.angle()
	if sign(velocity.x) == 1:
		$SpriteShip.flip_v = false
	else:
		$SpriteShip.flip_v = true
	$SpriteShipGun2.look_at(get_global_mouse_position())
	update()

func _draw() ->void:
	draw_line(Vector2.ZERO, desired_direction, Color.aquamarine)
	draw_line(Vector2.ZERO, velocity, Color.orange, 15.0)
	draw_line(velocity, desired_direction,Color.red, 5)
	draw_line(Vector2.ZERO, desired_velocity, Color.green, 30)



func _on_Timer_timeout() -> void:
	shoot()

func shoot():
	#add ship velocity to bullet so backing away isn't broken
	#shots should only collide at max speed
	#visual speed indicator shows when you're fast enough
	#but the speed has to be gun direction + ship direction velocity so if looking
	#back you shoot slower
	var laser_top = laser_scene.instance()
	laser_top.rotation = $SpriteShipGun2.rotation + 0.045
	laser_top.position = get_node("SpriteShipGun2/top_gun").global_position
	get_tree().get_root().add_child(laser_top)

	var laser_bot = laser_scene.instance()
	laser_bot.rotation = $SpriteShipGun2.rotation - 0.045
	laser_bot.position = get_node("SpriteShipGun2/bot_gun").global_position
	get_tree().get_root().add_child(laser_bot)
