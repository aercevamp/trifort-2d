extends Area2D


export var bullet_speed := 100
export var timer_time := 1
onready var timer := get_node("Timer")

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	timer.set_wait_time(timer_time)
	timer.one_shot = true
	timer.connect("timeout", self, "queue_free")
	timer.start()

func _physics_process(delta: float) -> void:
	move_local_x(bullet_speed * delta)

#if laser collids with core >> add to tree core explosion.tscn
