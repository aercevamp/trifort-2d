extends KinematicBody2D


export var default_max_speed := 300
onready var max_speed := default_max_speed
var acceleration = Vector2.ZERO
export var default_turn_speed := 150.0
onready var turn_speed := default_turn_speed
onready var steering_agent : Object = null
onready var raycasts: Node2D = get_node("Raycasts")
var speed := 300
var current_direction := Vector2.RIGHT
var desired_direction := Vector2.ZERO
var desired_velocity := Vector2.ZERO
var stored_velocity := Vector2.RIGHT
var velocity := Vector2.RIGHT
var thrust := true
var states := {}
var current_state := "idle"
var look_ahead := Vector2.RIGHT
var temp_norm

func set_steering_agent(agent):
	steering_agent = get_parent().get_node(agent)
	
func _ready() -> void:
	set_steering_agent("Player")
	
	
	
func _physics_process(delta: float) -> void:
	#do_state_stuff()
	current_direction = velocity.normalized()
	approach_state()
	desired_direction = (steering_agent.position - position).normalized() * max_speed
	var steer_result = avoid_steer()
	desired_direction = lerp(desired_direction, steer_result["modified_direction"], steer_result["percentage"])
	#desired_direction = desired_direction.normalized()
	if desired_direction != Vector2.ZERO:
		var angle = current_direction.angle_to(desired_direction)

		var angle_diff = clamp(angle, -0.1, 0.1) * turn_speed * delta
		#angle_diff = clamp(angle_diff, -abs(angle), abs(angle))

		current_direction = current_direction.rotated(angle_diff)

	if thrust == true: desired_velocity =  current_direction * speed
	else: desired_velocity = velocity.normalized() * speed
	var steering_vector := desired_velocity - velocity
	velocity += steering_vector * 0.1
	velocity = move_and_slide(velocity)
	
	#Ray
	look_ahead = lerp(Vector2.RIGHT, (steering_agent.position - position), 0.2).normalized()
	#? replacing the steering agent stuff with desired direction is more accurate but jitters
	#Visual
	$SpriteShip.rotation = velocity.angle()
	if sign(velocity.x) == 1:
		$SpriteShip.flip_v = false
	else:
		$SpriteShip.flip_v = true
	update()

func avoid_steer():
	raycasts.global_rotation = look_ahead.angle()
	for raycast in raycasts.get_children(): #3 raycasts is better but requires priority system
		if raycast.is_colliding():
			#use collision masks to have boxes for collision only? bigger than the actual object?
			if raycast.get_collider().is_in_group("Obstacle"):
				var obstacle: PhysicsBody2D = raycast.get_collider()
				var collision_normal = (raycast.get_collision_normal())
				var collision_angle = (raycast.get_collision_normal().angle_to(-look_ahead))
				#if positive then go that way if negative then go the other way!
				var perpendicular_normal = Vector2.RIGHT
				if sign(collision_angle) >= 0:
					perpendicular_normal = Vector2(collision_normal.y, -collision_normal.x)
				else:
					perpendicular_normal = Vector2(-collision_normal.y, collision_normal.x)
				var modified_direction = perpendicular_normal.normalized() * max_speed
				
				var proximity = (global_position - raycast.get_collision_point()).length()
				var adjusted_prox = clamp((raycast.cast_to/proximity).length() * 100, 0, 100) / 100
				#math not accurate! I want a percentage of the raycast, mb with some exponential modifier
				print(adjusted_prox)
				var avoid_steer_return := {"modified_direction": modified_direction, "percentage": adjusted_prox}
				return (avoid_steer_return)
					#position + velocity - raycast.get_collision_point()).normalized() * 700
	#Use collision_normal instead, the sharper the normal the more it turns so it doesn't vibrate
	#combine velocity & desired vector for "lookahead"
	#if touching at the peak make it perpendicular, more and it pushes further
#Why is 700 the number that works? It likes to crash into things at sharp angles under that
#Could make it more smooth? Also would be nice if it could predict if its current path
#(not just current velocity vector) could lead to a collision
#It fucks up when the forward ray gets triggered, runs right into the wall somehow
	return ( {"modified_direction": Vector2.ZERO, "percentage": 0.0})

func update_the_rays():
	pass

func _draw() ->void:
	draw_line(Vector2.ZERO, desired_direction, Color.orange)
	draw_line(Vector2.ZERO, velocity, Color.green)
	draw_line(Vector2.ZERO, look_ahead * 150, Color.purple)

func approach_state():
	var cleanthis = (steering_agent.position - position)#direction_to(steering_agent) not declared????
	if cleanthis.dot(current_direction) < 0:
		speed = 100
		turn_speed = 350
	else:
		speed = default_max_speed
		turn_speed = default_turn_speed
		#better with a state you enter then leave....
		#if you get around an obstacle and are behind the enemy they'll slow down and keep trying
		#to turn into the obstacle, maybe in this case tell them to try the other way around?
		#or make it so that they only behave this way away from obstacles
		#maybe the avoid collision can be a state that takes over, but I still need
		#to make sure it crashes into stuff easier when chasing, prob speed?
